// https://developer.gnome.org/gtk3/stable/gtk-getting-started.html

#include <gtk/gtk.h>

static void activate(GtkApplication *app, gpointer user_data) {
  GtkWidget *window;
  GtkWidget *scrollWindow;
  GtkWidget *textView;
  GtkWidget *vbox;

  window = gtk_application_window_new(app);
  gtk_window_set_title(GTK_WINDOW(window), "GTK3 IME Test");
  gtk_window_set_default_size(GTK_WINDOW(window), 600, 400);

  scrollWindow = gtk_scrolled_window_new(NULL, NULL);
  textView = gtk_text_view_new();

  gtk_container_add(GTK_CONTAINER(scrollWindow), textView);

  vbox = gtk_box_new(FALSE, 5);
  gtk_box_pack_start(GTK_BOX(vbox), scrollWindow, TRUE, TRUE, 6);

  gtk_container_add(GTK_CONTAINER(window), vbox);

  /* https://developer.gnome.org/gtk3/stable/gtk3-General.html */
  /* https://stackoverflow.com/questions/36905118/code-cannot-exit-from-gtk-application-apparently-no-message-loops
   */
  /* If you're calling g_application_run() then you don't need to call
     gtk_main() as well: the run() method will spin the main loop for
     you. */
  // g_signal_connect (window, "destroy",
  //                    G_CALLBACK (gtk_main_quit),
  //                    NULL);

  gtk_widget_show_all(window);
}

int main(int argc, char **argv) {
  GtkApplication *app;
  int status;

  app = gtk_application_new("org.gtk.example", G_APPLICATION_FLAGS_NONE);
  g_signal_connect(app, "activate", G_CALLBACK(activate), NULL);
  status = g_application_run(G_APPLICATION(app), argc, argv);
  g_object_unref(app);

  return status;
}
