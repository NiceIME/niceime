// https://developer.gnome.org/gtk-tutorial/stable/c39.html

#include <gtk/gtk.h>


int main(int argc, char *argv[]) {
  /* GtkWidget is the storage type for widgets */
  GtkWidget *window;
  GtkWidget *scrollWindow;
  GtkWidget *textView;
  GtkWidget *vbox;

  /* This is called in all GTK applications. Arguments are parsed
   * from the command line and are returned to the application. */
  gtk_init(&argc, &argv);

  /* create a new window */
  window = gtk_window_new(GTK_WINDOW_TOPLEVEL);

  gtk_window_set_title(GTK_WINDOW(window), "GTK2 IME Test");
  gtk_window_set_default_size(GTK_WINDOW(window), 600, 400);

  scrollWindow = gtk_scrolled_window_new(NULL, NULL);
  textView = gtk_text_view_new();

  gtk_container_add(GTK_CONTAINER(scrollWindow), textView);

  vbox = gtk_vbox_new(FALSE, 5);
  gtk_box_pack_start(GTK_BOX(vbox), scrollWindow, TRUE, TRUE, 6);

  gtk_container_add(GTK_CONTAINER(window), vbox);

  g_signal_connect(GTK_OBJECT(window), "destroy", G_CALLBACK(gtk_main_quit),
                   NULL);

  gtk_widget_show_all(window);

  gtk_main();

  return 0;
}
